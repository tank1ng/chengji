// 一维数组 -> 二维数组,分页功能
export const one2arr = (curryArr, pageSize) => {
  let j = 0;
  let data = [];
  for (let i = 0; i < curryArr.length; i++) {
    if (i % pageSize == 0) {
      j++;
      data[j - 1] = new Array();
    }
    data[j - 1][i % pageSize] = curryArr[i];
  }
  return data
}

export const curryArr = (arr) => {
  // 扁平化term,teaches.name
  let curryArr = []
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].Terms.length; j++) {
      let item = {};
      item.cid = arr[i].cid;
      item.name = arr[i].name;
      item.score = arr[i].score;
      item.term = (arr[i].Terms[j]).term;
      let teaches = (arr[i].Terms[j]).Teaches;
      let nameArr = []
      let idArr = []
      if (teaches.length == 0) { //  课程无老师，不显示
        continue;
      } else {
        for (let i = 0; i < teaches.length; i++) {
          nameArr.push(teaches[i].User.name)
          idArr.push(teaches[i].uid)
        }
        item.teacher = nameArr.join(',')
        item.uid = idArr.join(',')
        curryArr.push(item)
      }
    }
  }
  return curryArr
}

export const curryArr2 = (arr) => { // 不过滤无老师的课程
  // 扁平化term,teaches.name
  let curryArr = []
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].Terms.length; j++) {
      let item = {};
      item.cid = arr[i].cid;
      item.name = arr[i].name;
      item.score = arr[i].score;
      item.term = (arr[i].Terms[j]).term;
      let teaches = (arr[i].Terms[j]).Teaches;
      let nameArr = []
      let idArr = []
      for (let i = 0; i < teaches.length; i++) {
        nameArr.push(teaches[i].User.name)
        idArr.push(teaches[i].uid)
      }
      item.teacher = nameArr.join(',')
      item.uid = idArr.join(',')
      curryArr.push(item)
    }
  }
  return curryArr
}

export const curryTea = (arr) => {
  // 扁平化teachers
  let curryTea = [];
  for (let i = 0; i < arr.length; i++) {
    let tearr = arr[i].teacher.split(',')
    let idArr = arr[i].uid.split(',')
    for (let j = 0; j < tearr.length; j++) {
      let item = {};
      item.cid = arr[i].cid;
      item.name = arr[i].name;
      item.score = arr[i].score;
      item.term = arr[i].term;
      item.teacher = tearr[j]
      item.teaId = idArr[j]
      curryTea.push(item)
    }
  }
  return curryTea
}

export const item2Num = (arr) => {
  let _arr = []
  for (let i = 0; i < arr.length; i++) {
    _arr[i] = Number(arr[i])
  }
  return _arr
}