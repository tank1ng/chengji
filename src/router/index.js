import VueRouter from 'vue-router'
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error)
}
import Vue from 'vue'
Vue.use(VueRouter)

const routes = [{
    path: "/",
    redirect: '/login',
    meta: {
      title: "欢迎来到学生成绩管理系统"
    }
  },
  {
    path: '',
    redirect: '/login'
  }, {
    path: '/login',
    meta: {
      title: "登陆"
    },
    component: () => import('views/login/index')
  }, {
    path: '/admin',
    meta: {
      title: "主页"
    },
    component: () => import('views/admin/index'),
    children: [{
        path: 'course',
        meta: {
          title: "课程管理"
        },
        component: () => import('views/admin/course/index')
      },
      {
        path: 'teacher',
        meta: {
          title: "教师管理"
        },
        component: () => import('views/admin/teacher/index')
      },
      {
        path: 'student',
        meta: {
          title: "学生管理"
        },
        component: () => import('views/admin/student/index')
      },
    ]
  },

  {
    path: '/teacher',
    meta: {
      title: "主页"
    },
    component: () => import('views/teacher/index'),
    children: [{
        path: 'info',
        meta: {
          title: '教师信息'
        },
        component: () => import('views/teacher/info')
      },
      {
        path: 'courseList',
        meta: {
          title: "我的课程"
        },
        component: () => import('views/teacher/courseList')
      },
      {
        path: 'courseDetail/:cid',
        meta: {
          title: "课程详情"
        },
        component: () => import('views/teacher/courseDetail')
      }
    ]
  },


  {
    path: '/student',
    meta: {
      title: "主页"
    },
    component: () => import('views/student/index'),
    children: [{
        path: 'info',
        meta: {
          title: "个人信息"
        },
        component: () => import('views/student/info'),
      },
      {
        path: 'myCourse',
        meta: {
          title: "我的课程"
        },
        component: () => import('views/student/myCourse')
      },
      {
        path: 'chooseCourse',
        meta: {
          title: "选课"
        },
        component: () => import('views/student/chooseCourse')
      }
    ]
  },
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router