import service from './index'

// 学生查询所有课程
export function checkAllCourse(){
  return service.get('/v1/student/allClass')
}
// 查看自己的选课
export function checkSelfCourse(){
  return service.get('/v1/student/class')
}

// 学生选课
export function takeClass(cid,term,uid){
  return service.post(`/v1/student/takeclass/${cid}/${term}/${uid}`)
}

// 查看某一门课成绩
export function checkGrade(cid,term,uid){
  return service.get(`/v1/student/grade/${cid}/${term}/${uid}`)
} 