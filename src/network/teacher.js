import service from './index'

// 教师查看自己的课程
export function teacherCheckCourse(){
  return service.get('/v1/teacher/course')
}

// 教师查看某一门课程下的学生(按学期查询)
export function teacherCheckStudent(cid,term){
  return service.get('/v1/teacher/course/student/'+cid+'/'+term)
}

// 修改成绩(某一门课+某一学期)
export function teacherPutGrade(cid,term,uid,grade){
  //分为两种情况,一种是已经有成绩了,需要修改,一种是没有成绩,但是选了这门课程
  return service.put(`/v1/teacher/course/student/${cid}/${term}/${uid}/${grade}`)
}
