import service from './index'

// 管理员查看所有课程
export function courseCheck() {
  return service.get('/v1/admin/course')
}

// 添加课程
export function courseAdd(name, score, term) {
  return service.post('/v1/admin/course', {
    name,
    score,
    term
  })
}

// 修改课程
export function courseModify(cid, score, name, term) {
  return service.put('v1/admin/course', {
    cid, //4,
    score, //3,
    name, //"数据结构与算法",
    term //[1,5,6]
  })
}

// 删除课程
export function courseDelete(id) {
  return service.delete('v1/admin/course/' + id)
}

// 查看所有老师
export function teacherCheck() {
  return service.get('/v1/admin/teacher')
}

// 添加老师
export function teacherAdd(uid, password, name, cid, term) {
  return service.post('/v1/admin/teacher', {
    uid, // '201700010',
    name, // "潘sir",//可选
    password, // "123456",//可选 传入则修改,不传则不变
    cid, // 2,//可选,若传入cid 则必须一并传入term
    term // [4]
  })
}

// 删除老师
export function teacherDel(uid) {
  return service.delete('/v1/admin/teacher/' + uid)
}

// 修改老师
export function teacherPut(uid, name) {
  return service.put('/v1/admin/teacher', {
    uid, //"201700010"//除了uid 其余为可选参数,term和cid必须一并传入才有效果
    name, //"潘勇浩",
  })
}

// 修改老师(包含修改任课信息)
export function teacherPutAll(uid, name, cid, term) {
  return service.put('/v1/admin/teacher', {
    name, //"潘勇浩",
    uid, //"201700010",//除了uid 其余为可选参数,term和cid必须一并传入才有效果
    cid, //2,
    term //[4,5]
  })
}

// 查看所有学生
export function studentCheck() {
  return service.get('/v1/admin/student')
}

// 添加学生
export function studentAdd(uid, password, name) {
  return service.post('/v1/admin/student', {
    uid,
    password,
    name
  })
}

//删除学生
export function studentDel(uid) {
  return service.delete('v1/admin/student/' + uid)
}

// 修改学生
export function studentPut(uid, password) {
  return service.put('/v1/admin/student', {
    uid,
    password
  })
}

// 修改学生(包含修改成绩)
export function studentPutAll(uid, tid, cid, term, grade) {
  return service.put('/v1/admin/student', {
    uid, //"201702772",
    tid, //"201700001",
    cid, //3,
    term, //1,
    grade, //99
  })
}