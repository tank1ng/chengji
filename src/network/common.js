import service from './index'

// 登陆
export function login(uid,password){
  return service.post('/v1/token',{
    uid,
    password
  })
}

// 查看用户信息
export function Usercheck(uid){
  return service.get('/v1/user',{
    params:{
      uid
    }
  })
}
