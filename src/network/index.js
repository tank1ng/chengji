import axios from 'axios'

const service = axios.create({
  baseURL: "http://www.nylv.xyz:4500"
})

//请求拦截器
service.interceptors.request.use(config => {
  config.headers = Object.assign({}, config.headers, {
    token: sessionStorage.getItem('token')
  })
  return config
})
//响应拦截器
service.interceptors.response.use(res => {
  console.log(res);
  if (res.data.code == 200) {
    return res.data
  } else {
    //做全局处理错误异常
    return res.data
  }
})

export default service
